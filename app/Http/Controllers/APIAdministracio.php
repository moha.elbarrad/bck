<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use App\Empresa;
class APIAdministracio extends Controller
{
    public function validarUser($id){
        $usuari = User::where('usuari', '=', $id)->get()->first();
        return response()->json($usuari);
    }
    public function login($id){
        $ids = explode("-", $id);
        $usuari = User::where('idUsuari', '=', $ids[0])->get()->first();
        if($usuari && Hash::check($ids[1], $usuari->password)){
            return response()->json($usuari);
        }
    }

    public function indexEmpresa(){
        return response()->json(Empresa::all());
    }
}
