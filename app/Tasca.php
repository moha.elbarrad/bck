<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasca extends Model
{
    protected $table = 'tasques';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idServidor',
        'nomTasca'
    ];

    protected $primaryKey = 'idTasca';

    public function tasques(){
        return $this->belongsTo('App\Servidor');
    }
}
