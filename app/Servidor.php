<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servidor extends Model
{
    protected $table = 'servidors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idEmpresa',
        'nomServidor',
        'ipServidor'
    ];

    protected $primaryKey = 'idServidor';

    public function servidors(){
        return $this->belongsTo('App\Empresa');
    }
}
