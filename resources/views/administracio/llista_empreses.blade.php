@extends('layouts.dashboardAdmin')

@section('content')
    <div id="app">
        <llistat-empreses :ruta="'{{ env('APP_URL') }}'"></llistat-empreses>
    </div>
@endsection