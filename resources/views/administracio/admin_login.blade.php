@extends('layouts.dashboard')

@section('content')
    <div id="app">
        <admin-component :ruta="'{{ env('APP_URL') }}'"></admin-component>
    </div>
@endsection