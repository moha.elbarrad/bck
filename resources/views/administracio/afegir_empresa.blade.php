@extends('layouts.dashboardAdmin')

@section('content')
<div class="container h-100">
    <div class="d-flex justify-content-center h-100">
        <div class="user_card">
            <div class="d-flex justify-content-center">
                <img src="{{ asset('img/logo.png') }}" class="brand_logo" alt="Logo">
            </div>
            <form method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="nomEmpresa" class="col-md-4 col-form-label text-md-right">{{ __('Nom i cognoms') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection