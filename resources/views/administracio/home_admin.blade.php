@extends('layouts.dashboardAdmin')

@section('content')
	<div id="app">
		<adminhome-component :ruta="'{{ env('APP_URL') }}'"></adminhome-component>
	</div>
@endsection
