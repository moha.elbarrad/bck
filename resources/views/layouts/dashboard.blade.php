<!DOCTYPE html>
<html>
    <head>
        <title>SITIC Solutions</title>
        <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
        <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
        <link rel="stylesheet" href="{{ asset('css/login.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    </head>
    <body>
        <div class="wrapper d-flex align-items-stretch">
            <nav id="sidebar">
                <div class="custom-menu">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary">
                        <i class="fa fa-bars"></i>
                            <span class="sr-only">Toggle Menu</span>
                    </button>
                </div>
                <div class="p-4 pt-5">
                <h3><a href="{{ route('home')}}">SITIC Solutions</a></h3>
                    <ul class="list-unstyled components mb-5">
                        <li id="perfil">
                            <img id="imatgePerfil" src="{{ asset('img/luffy.jpg')}}"/>
                            <p>Mohmed el barrad</p>
                        </li>
                        <li>
                            <a href="{{ route('revisarC')}}">Revisar còpia</a>
                        </li>
                        <li>
                            <a>Estat còpies</a>
                        </li>
                        <li>
                            <a href="{{ route('admin')}}">Administració</a>
                        </li>
                        <li>
                            <a href="">Perfil</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                                      {{ __('Logout') }}
                            </a>
      
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </nav>
          
            <!-- Page Content  -->
            <div id="content" class="p-4 p-md-5 pt-5">
                <!--div class="container"-->
                <!--@notification()-->
                @yield('content')
                <!--/div-->
            </div>
        </div>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="{{asset('js/dashboard.js')}}"></script>
    </body>
</html>