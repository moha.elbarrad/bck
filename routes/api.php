<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get("/allUsers", 'APIUsers@index');
Route::get("/usuari/{id}", 'APIUsers@usuariById');

Route::get("/allEmpreses", 'APIAdministracio@indexEmpresa');

Route::get("/validarAdminUser/{id}", 'APIAdministracio@validarUser');
Route::get("/loginAdminUser/{id}", 'APIAdministracio@login');