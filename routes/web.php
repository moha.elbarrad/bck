<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/', function() {
    return view('auth/login');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/revisarCopia', 'HomeController@revisar')->name('revisarC');
Route::get('/LoginAdministracio', 'AdminController@login')->name('admin');
Route::get('/homeAdmin', 'AdminController@index')->name('homeAdmin');
Route::get('/llistatEmpreses', 'AdminController@indexEmpresa')->name('llistatEmpreses');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/revisarCopia', 'HomeController@revisar')->name('revisarC');
Route::get('/LoginAdministracio', 'AdminController@login')->name('admin');
Route::get('/homeAdmin', 'AdminController@index')->name('homeAdmin');
Route::get('/llistatEmpreses', 'AdminController@indexEmpresa')->name('llistatEmpreses');