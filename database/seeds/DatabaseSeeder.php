<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        self::seedUsers();
        $this->command->info('Taula users inicialitzada amb dades!');
    }

    private function seedUsers(){
        DB::table('users')->delete();

        foreach($this->arrayUsers as $user){
            $u = new User;
            $u->name = $user['name'];
            $u->usuari = $user['usuari'];
            $u->email = $user['email'];
            $u->password = bcrypt($user['password']);
            $u->tipusUsuari = $user['tipusUsuari'];
            $u->save();
        }
    }

    private $arrayUsers = array(
        array(
            'name' => 'Administrador',
            'usuari' => 'admin',
            'email' => 'admin@sitic.cat',
            'password' => 'reverse',
            'tipusUsuari' => '0'
        ),
        array(
            'name' => 'Josep Marti',
            'usuari' => 'jmarti',
            'email' => 'jmarti@sitic.cat',
            'password' => 'reverse',
            'tipusUsuari' => '1'
        ),
        array(
            'name' => 'Mohamed El Barrad',
            'usuari' => 'melbarrad',
            'email' => 'melbarrad@sitic.cat',
            'password' => 'reverse',
            'tipusUsuari' => '2'
        )
    );
}
