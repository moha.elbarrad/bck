<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServidorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servidors', function (Blueprint $table) {
            $table->bigIncrements('idServidor');
            $table->unsignedBigInteger('idEmpresa')->unsigned();
            $table->string('nomServidor');
            $table->string('ipServidor');
            $table->foreign("idEmpresa")->references('idEmpresa')->on('empreses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servidors');
    }
}
